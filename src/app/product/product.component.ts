import { Component } from '@angular/core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent {
  Name = "somesh";
  time: string ="1pm";

  isAvailable: boolean = true; 
  
  product = {
    name:'T.V.',
    description:'The Cognitive Processor XR™ delivers a picture with wide dynamic contrast and natural colors, replicating how we see the real world. Get access to all your favorite streaming apps in one place with Google TV™, and simply use your voice to search and ask questions with Google Assistant™. Supports Apple Airplay. Get high-quality 4K UHD movies on us with the BRAVIA CORE™ app. Game Menu puts all your gaming picture settings and exclusive assist features in one place. Take your PlayStation® 5 gaming to the next level with exclusive features that optimize picture quality for gaming and streaming.',
    stock:'10',
    image:'/assets/my_product.jpg'
  }
  
  whenchanged(event:any){
    // this.Name = event.target.value

    //console.log(event.target.value)

    this.time = event.target.value
    
  }


  isyellow = true

  
  

}

